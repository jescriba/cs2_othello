//final commit
#include "board.h"

/*
 * Make a standard 8x8 othello board and initialize it to the standard setup.
 */
Board::Board() {
    taken.set(3 + 8 * 3);
    taken.set(3 + 8 * 4);
    taken.set(4 + 8 * 3);
    taken.set(4 + 8 * 4);
    black.set(4 + 8 * 3);
    black.set(3 + 8 * 4);
}

/*
 * Destructor for the board.
 */
Board::~Board() {
}

/*
 * Returns a copy of this board.
 */
Board *Board::copy() {
    Board *newBoard = new Board();
    newBoard->black = black;
    newBoard->taken = taken;
    return newBoard;
}

bool Board::occupied(int x, int y) {
    return taken[x + 8*y];
}

bool Board::get(Side side, int x, int y) {
    return occupied(x, y) && (black[x + 8*y] == (side == BLACK));
}

void Board::set(Side side, int x, int y) {
    taken.set(x + 8*y);
    black.set(x + 8*y, side == BLACK);
}

bool Board::onBoard(int x, int y) {
    return(0 <= x && x < 8 && 0 <= y && y < 8);
}

 
/*
 * Returns true if the game is finished; false otherwise. The game is finished 
 * if neither side has a legal move.
 */
bool Board::isDone() {
    return !(hasMoves(BLACK) || hasMoves(WHITE));
}

/*
 * Returns true if there are legal moves for the given side.
 */
bool Board::hasMoves(Side side) {
    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {
            Move move(i, j);
            if (checkMove(&move, side)) return true;
        }
    }
    return false;
}

/*
 * Returns true if a move is legal for the given side; false otherwise.
 */
bool Board::checkMove(Move *m, Side side) {
    // Passing is only legal if you have no moves.
    if (m == NULL) return !hasMoves(side);

    int X = m->getX();
    int Y = m->getY();

    // Make sure the square hasn't already been taken.
    if (occupied(X, Y)) return false;

    Side other = (side == BLACK) ? WHITE : BLACK;
    for (int dx = -1; dx <= 1; dx++) {
        for (int dy = -1; dy <= 1; dy++) {
            if (dy == 0 && dx == 0) continue;

            // Is there a capture in that direction?
            int x = X + dx;
            int y = Y + dy;
            if (onBoard(x, y) && get(other, x, y)) {
                do {
                    x += dx;
                    y += dy;
                } while (onBoard(x, y) && get(other, x, y));

                if (onBoard(x, y) && get(side, x, y)) return true;
            }
        }
    }
    return false;
}

/*
 * Modifies the board to reflect the specified move.
 */
void Board::doMove(Move *m, Side side) {
    // A NULL move means pass.
    if (m == NULL) return;

    // Ignore if move is invalid.
    if (!checkMove(m, side)) return;

    int X = m->getX();
    int Y = m->getY();
    Side other = (side == BLACK) ? WHITE : BLACK;
    for (int dx = -1; dx <= 1; dx++) {
        for (int dy = -1; dy <= 1; dy++) {
            if (dy == 0 && dx == 0) continue;

            int x = X;
            int y = Y;
            do {
                x += dx;
                y += dy;
            } while (onBoard(x, y) && get(other, x, y));

            if (onBoard(x, y) && get(side, x, y)) {
                x = X;
                y = Y;
                x += dx;
                y += dy;
                while (onBoard(x, y) && get(other, x, y)) {
                    set(side, x, y);
                    x += dx;
                    y += dy;
                }
            }
        }
    }
    set(side, X, Y);
}

/*
 * Current count of given side's stones.
 */
int Board::count(Side side) {
    return (side == BLACK) ? countBlack() : countWhite();
}

/*
 * Current count of black stones.
 */
int Board::countBlack() {
    return black.count();
}

/*
 * Current count of white stones.
 */
int Board::countWhite() {
    return taken.count() - black.count();
}

std::vector<Move*> Board::possible_moves(Side side){
    std::vector<Move*> move_list;
    for(int i = 0; i < 8; i++)
    {
        for(int k = 0; k < 8; k ++)
        {
            Move *option = new Move(i, k); 
            if(checkMove(option, side))
            {
                move_list.push_back(option);  
            }
        }
    }
    return move_list;
}

// Is it a corner or good edge piece?

bool corner(Move *move)
{
    if((move->getX() == 0 and move->getY() == 0) or \
       (move->getX() == 0 and move->getY() == 7) or \
       (move->getX() == 7 and move->getY() == 0) or \
       (move->getX() == 7 and move->getY() == 7))
    {
        return true; 
    }
    if((move->getX() == 0 and (move->getY() > 1 and move->getY() < 6)) or \
    (move->getX() == 7 and (move->getY() > 1 and move->getY() < 6)) or \ 
    (move->getY() == 0 and (move->getX() > 1 and move->getX() < 6)) or \
    (move->getY() == 7 and (move->getX() > 1 and move->getX() < 6)))
    {
        return true; 
    }
    return false; 
}

// Is it a "bad" edge piece?
 
bool unfavorable(Move *move)
{
	if((move->getX() == 0 and (move->getY() == 1 or move->getY() == 6)) or \
    (move->getX() == 7 and (move->getY() == 1 or move->getY() == 6)) or \ 
    (move->getY() == 0 and (move->getX() == 1 or move->getX() == 6)) or \
    (move->getY() == 7 and (move->getX() == 1 or move->getX() == 6)))
    {
        return true; 
    }
    return false; 
}

Move * Board::Maximum(Side side)
{
    int score, top_score = -1000;
    Move * top_move = new Move(1, 1); 
    std::vector<Move *> move_list = this->possible_moves(side);
    std::vector<Move *>::iterator it;
    for(it = move_list.begin(); it != move_list.end(); it++)
    {
        Move *move = *it;
        Board *temp_board = new Board();
        temp_board = this->copy(); 
        temp_board->doMove(move, side);
        score = temp_board->score(side);
        // Heuristic
        //if(corner(move)){score += 1;}
        //if(unfavorable(move)){score -= 1;}
        if(score >= top_score)
        {
            top_score = score;
            top_move->setX(move->getX()); 
            top_move->setY(move->getY());
        }
    }
    return top_move;
}

int Board::score(Side side)
{
    Side other = (side == BLACK) ? WHITE : BLACK;
    int score;
    score = this->count(side) - this->count(other); 
    return score; 
}

data Board::evaluate(Side side, Side max_side)
{
     Side me = side; 
     data top_result;
     Board * temp_board = new Board(); 
     temp_board = this->copy(); 
     Move * top_move = temp_board->Maximum(max_side);
     temp_board->doMove(top_move, max_side);
     top_result.me = temp_board->score(me);
     top_result.computer = -1 * top_result.me; 
     top_result.move = top_move;
     return top_result;
}
