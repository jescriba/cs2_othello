// final commit. unfortunately, didn't finish all that i wanted to ;/
#include "exampleplayer.h"
/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */

Side me;
Side computer;  
Board *board = new Board();

ExamplePlayer::ExamplePlayer(Side side) {
    /* 
     * TODO: Do any initialization you need to do here (setting up the board,
     * precalculating things, etc.) However, remember that you will only have
     * 30 seconds.
     */
    // global variable for whos on what team
    if(side == BLACK)
    {
        computer = WHITE;
        me = BLACK;
    }
    else
    {
        computer = BLACK; 
        me = WHITE;
    }
    // copy initial setup
    board->copy();  
}

/*
 * Destructor for the player.
 */
ExamplePlayer::~ExamplePlayer() {
}


/*
 * Compute the next move given the opponent's last move. Each AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * If there are no valid moves for your side, doMove must return NULL.
 *
 * Important: doMove must take no longer than the timeout passed in 
 * msLeft, or your AI will lose! The move returned must also be legal.
 */
Move *ExamplePlayer::doMove(Move *opponentsMove, int msLeft) {
    /* 
     * TODO: Implement how moves your AI should play here. You should first
     * process the opponent's opponents move before calculating your own move
     */ 
    if(opponentsMove != NULL)
    {
        board->doMove(opponentsMove, computer);
    }
    if(board->hasMoves(me) == false)
    {
        return NULL;
    }
    else
    {
        Move * top_move = new Move(1, 1); 
        Board * temp_board = new Board(); 
        temp_board = board->copy(); 
        top_move = Minimax(temp_board, 5, 0, -100000, 100000);
        board->doMove(top_move, me);
        std::cerr<< "(" << top_move->getX() << ", " << top_move->getY() << ")" << std::endl;
        return top_move; 
    }
    return NULL;
}

// Minimax Algorithm 

Move * Minimax(Board * game, int depth, int current, int a, int b)
{
	Move * top_move = Max(game, depth, current, a, b).move;
	return top_move; 
}

data Max(Board * game, int depth, int current, int a, int b)
{
	current += 1;
	if(game->isDone() or current == depth)
	{
		return game->evaluate(me, me);
	}
	else
	{
		int top_score = -100000;
		data top_data; 
		Move * top_move = new Move(1, 1); 
		std::vector<Move*> moves = game->possible_moves(me);
		if(moves.size() == 0){return game->evaluate(me, me);}
		std::vector<Move*>::iterator it; 
		for(it = moves.begin(); it != moves.end(); it++)
		{ 
			Move *move = *it; 
			Board * temp = new Board();
			temp = game->copy();
			temp->doMove(move, me);
			data result = Min(temp, depth, current, a, b);
			// I try to maximize my score
			if(result.me > top_score)
			{
				top_move->setX(move->getX());
				top_move->setY(move->getY());
				top_score = result.me; 
				top_data.me = top_score;
		        top_data.computer = -1 * top_score;
		        top_data.move = top_move;
		        // Assign alpha for pruning
		        a = result.me; 
			}
			// Prune if criteria met
			if(b < a)
			{
				//std::cerr << "Pruned Max" << std::endl;
				break;
			}
		}
		return top_data; 
	}
}

data Min(Board * game, int depth, int current, int a, int b)
{
	current += 1;
	if(game->isDone() or current == depth)
	{
		return game->evaluate(me, computer);
    }
    else
    {
		int top_score = -100000;
		data top_data;
		data result;
		Move * top_move = new Move(1, 1);
		std::vector<Move*> moves = game->possible_moves(computer);
		if(moves.size() == 0){return game->evaluate(me, computer);}
		std::vector<Move*>::iterator it;
		for(it = moves.begin(); it != moves.end(); it++)
		{
			Move *move = *it; 
			Board *temp = new Board();
			temp = game->copy();
			temp->doMove(move, computer);
			result = Max(temp, depth, current, a, b);
			// Computer tries to maximize their score
			if(result.computer > top_score)
			{
				top_move->setX(move->getX());
				top_move->setY(move->getY());
				top_score = result.computer; 
				top_data.me = -1 * result.computer;
	            top_data.computer = result.computer;
	            top_data.move = top_move; 
	            // Assign beta for pruning 
	            b = result.me; 
		    }
		    // Prune if criteria met
		    if(b < a)
		    {
				//std::cerr << "Pruned Min" << std::endl;
				break;
		    }
	    }
	    return top_data;   
    }
}
