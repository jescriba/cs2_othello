//final commit
#ifndef __BOARD_H__
#define __BOARD_H__

#include <bitset>
#include <iostream>
#include <stdlib.h>
#include "common.h"
#include <vector>
using namespace std;

struct data
{
    int me;
    int computer;
    Move * move; 
};

class Board {
   
private:
    bitset<64> black;
    bitset<64> taken;    
       
    bool occupied(int x, int y);
    bool get(Side side, int x, int y);
    void set(Side side, int x, int y);
    bool onBoard(int x, int y);
      
public:
    Board();
    ~Board();
    Board *copy();
    bool isDone();
    bool hasMoves(Side side);
    bool checkMove(Move *m, Side side);
    void doMove(Move *m, Side side);
    int count(Side side);
    int countBlack();
    int countWhite();
    int score(Side side);
    std::vector<Move*> possible_moves(Side side);
    Move* Maximum(Side side);
    data evaluate(Side side, Side max_size); 
};

bool corner(Move * move); 
bool unfavorable(Move * move); 

#endif
