I worked solo. I spent most of my time debugging the minimax with the
alpha beta to get my AI to beat the given AIs from the assignment. The 
other improvements I made were heuristic to value appropriate edge and corner
pieces. However, I didn't find the appropriate balance to allow their 
influences to contribute to the performance of the AI. These improvements
would ideally allow for benefits that could be unforeseen past the depth 
of the minimax search tree to help the AI perform slightly better. Also, 
wanted to add a mobility measure but it was a bit difficult to incorporate
into my exisiting methods at the time.  
