//final commit
#ifndef __EXAMPLEPLAYER_H__
#define __EXAMPLEPLAYER_H__

#include <iostream>
#include <stdlib.h>
#include "common.h"
#include "board.h"
using namespace std;

class ExamplePlayer {

public:
    ExamplePlayer(Side side);
    ~ExamplePlayer();
    
    Move *doMove(Move *opponentsMove, int msLeft);
};

Move * Minimax(Board * game, int depth, int current, int a, int b);
data Max(Board * game, int depth, int current, int a, int b);
data Min(Board * game, int depth, int current, int a, int b);

#endif
